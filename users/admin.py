from django.contrib import admin

from users.models import Profile, ProfileTree, ApproveUsersTree
from treenode.admin import TreeNodeModelAdmin
from treenode.forms import TreeNodeForm


class ProfileTreeAdmin(TreeNodeModelAdmin):
    treenode_display_mode = TreeNodeModelAdmin.TREENODE_DISPLAY_MODE_ACCORDION
    form = TreeNodeForm


class ApproveUsersTreeAdmin(TreeNodeModelAdmin):
    treenode_display_mode = TreeNodeModelAdmin.TREENODE_DISPLAY_MODE_ACCORDION
    form = TreeNodeForm


admin.site.register(ProfileTree, ProfileTreeAdmin)
admin.site.register(Profile)
admin.site.register(ApproveUsersTree, ApproveUsersTreeAdmin)
# Register your models here.
