# Generated by Django 3.0.1 on 2020-07-26 09:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_auto_20200522_0133'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='profile',
            options={'permissions': [('buyer', 'Специалист по закупкам'), ('lawyer', 'Юрист'), ('accountant', 'Бухгалтер')]},
        ),
    ]
