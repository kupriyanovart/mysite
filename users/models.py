from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from treenode.models import TreeNodeModel
from django.utils.translation import gettext_lazy as _


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    patronymic = models.CharField(max_length=150, blank=True)
    position = models.CharField(max_length=150, blank=True)
    img = models.ImageField(default='default.jpg', upload_to='user_images')

    class Meta:
        permissions = [('buyer', 'Специалист по закупкам'), ('lawyer', 'Юрист'),
                       ('accountant', 'Бухгалтер')]

    def __str__(self):
        return f'Профиль пользователя {self.user.username}'


class ProfileTree(TreeNodeModel):
    class ProfileTreeChoices(models.TextChoices):
        DIRECTOR = '0', _('Директор')
        HEAD_ENGINEER = '1', _('Главный инженер')
        AHO = '2', _('АХО')
        ENERGO = '3', _('Служба главного энергетика')
        TEPLO = '4', _('Служба главного теплотехника')
        ELECTRO = '5', _('Служба главного электроника')
        REMONT = '6', _('Отдел обслуживания и ремонта здания')
        MECHANIC = '7', _('Служба главного механика')
        CHIEF_ACCOUNTANT = '8', _('Главный бухгалтер')
        ACOUNTANT = '9', _('Бухгалтерия')
        DEPUTY_DIRETOR_LAW = '10', _('Заместитель директора по правовым и кадровым вопросам')
        LAWYER = '11', _('Юридический отдел')
        HR = '12', _('Отдел кадров')
        DEPUTY_DIRETOR_ORG = '13', _('Заместитель директора по организационным вопросам')
        ORG = '14', _('Организационный отдел')

    treenode_display_field = 'user'

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(verbose_name='Имя', max_length=150)
    last_name = models.CharField(verbose_name='Фамилия', max_length=150)
    position = models.CharField(verbose_name='Должность', max_length=150)
    department = models.CharField(
        max_length=50,
        choices=ProfileTreeChoices.choices,
    )


class ApproveUsersTree(TreeNodeModel):
    class ApproveUsersTreeChoices(models.TextChoices):
        DIRECTOR = '0', _('Директор')
        ACCOUNTANT = '1', _('Главный бухгалтер')
        ECONOMIST = '2', _('Экономист')
        LAWYER = '3', _('Начальник контрактной службы')

    treenode_display_field = 'user'

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(verbose_name='Имя', max_length=150)
    last_name = models.CharField(verbose_name='Фамилия', max_length=150)
    position = models.CharField(verbose_name='Должность', max_length=150)
    department = models.CharField(
        max_length=50,
        choices=ApproveUsersTreeChoices.choices,
    )
