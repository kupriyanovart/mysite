from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import Profile


@receiver(post_save, sender=User)
def create_profile(sender, **kwargs):

    if kwargs['created']:
        user_profile = Profile.objects.create(user=kwargs['instance'])

# TODO: Создать функцию ресивер для изменения профиля