from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import RegistrationForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required


def register(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Пользователь {username} успешно зарегистрирован! Введите имя пользователя и пароль для авторизации')
            return redirect('login')
    else:
        form = RegistrationForm()
    return render(request, 'users/registration.html',
                  {
                      'form': form,
                      'title': 'Регистрация пользователя'
                  })

@login_required
def profile(request):
    user_update = UserUpdateForm()
    profile_update = ProfileUpdateForm()

    context = {
        'user_update': user_update,
        'profile_update': profile_update
        }
    return render(request, template_name='users/profile.html', context=context)