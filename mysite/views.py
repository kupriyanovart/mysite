from django.shortcuts import render


def main_page(request):

    template = 'purchase/main_page.html'
    return render(request, template)