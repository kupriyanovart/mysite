import os
from pprint import pprint

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from mysite.settings import MEDIA_ROOT
from users.models import ProfileTree
from django.contrib.auth.models import User


# Create your models here.


class PurchaseRequest(models.Model):

    def purchase_file_path(self, filename):
        return f'purchase_files/{self.pk}/{filename}'

    class StatusChoices(models.TextChoices):
        CREATED = '0', _('Создана')
        TO_CONFIRM = '1', _('На согласовании')
        TO_APPROVE = '2', _('На утверждении')
        APPROVED = '3', _('Утверждена')
        MODIFY = '4', _('Отклонена')
        FIXED = '5', _('На согласовании после доработки')

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, verbose_name='Наименование объекта закупки')
    goods = models.TextField(verbose_name='Спецификация товаров, работ, услуг')
    supplier = models.CharField(max_length=200, verbose_name='Наименование поставщика')
    offer = models.FloatField(verbose_name='Ценовое предложение поставщика')
    apply_date = models.DateTimeField(auto_now_add=True)
    last_action_date = models.DateTimeField(default=timezone.now)
    status = models.CharField(
        max_length=50,
        choices=StatusChoices.choices,
        default=StatusChoices.CREATED
    )

    # files = models.FileField(verbose_name='Файлы', null=True, blank=True, upload_to=purchase_file_path)

    def __str__(self):
        return self.title

    #  При создании новой записи в БД возвращает url новой записи (для редиректа на страницу с новой закупкой)
    def get_absolute_url(self):
        return reverse('purchase:detail', kwargs={'pk': self.pk})

    def is_approved(self):
        if self.status == self.StatusChoices.TO_APPROVE:
            return True
        approves = PurchaseApprove.objects.filter(purchase=self.pk)
        for approve in approves:
            if approve.approver_choice == '' or approve.approver_choice == PurchaseApprove.ApproveChoices.values[1]:
                return False
        if self.status == self.StatusChoices.TO_CONFIRM or self.status == self.StatusChoices.FIXED:
            self.status = self.StatusChoices.TO_APPROVE
            self.save()
        return True

    def save(self, *args, **kwargs):
        # Note that change is False at the very first time
        update_fields = []
        if kwargs:
            for key, value in kwargs['form'].cleaned_data.items():
                # True if something changed in model
                if value != kwargs['form'].initial[key]:
                    update_fields.append(key)
            super().save(update_fields=update_fields)
        else:
            super().save()


class PurchaseRequestFiles(models.Model):

    def get_filename(self):
        s = self.file.name.split('/')
        return s[-1]

    purchase = models.ForeignKey(PurchaseRequest, db_index=True, on_delete=models.CASCADE)
    file = models.FileField(upload_to='purchase_files/%Y/%m/%d/', verbose_name='Файлы')


class PurchaseApprove(models.Model):
    ApproveChoices = models.TextChoices('Решение по закупке', 'Согласована Отклонена')

    purchase = models.ForeignKey(PurchaseRequest, on_delete=models.CASCADE)
    approver = models.ForeignKey(ProfileTree, on_delete=models.PROTECT)
    approver_choice = models.CharField(blank=True, choices=ApproveChoices.choices, max_length=20)
