# Generated by Django 3.0.5 on 2020-04-28 09:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('purchase', '0005_auto_20200427_2243'),
    ]

    operations = [
        migrations.CreateModel(
            name='PurchaseApprove',
            fields=[
                ('purchase', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='purchase.PurchaseRequest')),
                ('author_supervisor_choice', models.CharField(blank=True, choices=[('Согласована', 'Согласована'), ('Отклонена', 'Отклонена'), ('Согласование', 'Согласование')], max_length=20)),
                ('author_supervisor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
