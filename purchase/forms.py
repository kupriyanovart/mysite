from crispy_forms.helper import FormHelper
from django import forms
from django.forms import widgets

from purchase.models import PurchaseRequestFiles


class ApproveForm(forms.Form):
    comment = forms.CharField(required=False, label='Комментарий',
                              widget=forms.Textarea(attrs={
                                "rows": 4,
                                "cols": 50,
                                "class": "form-control"
                                }))


class PurchaseCreateForm(forms.Form):
    files = forms.FileField(label='Прикрепить файлы', widget=widgets.FileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        super(PurchaseCreateForm, self).__init__(*args, **kwargs)
        #self.helper = FormHelper()

    def clean_files(self):
        files = [f for f in self.request.FILES.getlist('files')]
        return files

    def save_for(self, purchase):
        for file in self.clean_files():
            PurchaseRequestFiles(purchase=purchase, file=file).save()
