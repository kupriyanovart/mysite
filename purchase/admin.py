from django.contrib import admin
from .models import PurchaseRequest, PurchaseApprove, PurchaseRequestFiles


class PurchaseRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'supplier', 'offer', 'last_action_date', 'author', 'status']
    list_display_links = ['id', 'title']


admin.site.register(PurchaseRequest, PurchaseRequestAdmin)
admin.site.register(PurchaseApprove)
admin.site.register(PurchaseRequestFiles)



