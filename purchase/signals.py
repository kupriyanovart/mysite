from pprint import pprint

from django.contrib.auth.models import User

from purchase.models import PurchaseApprove, PurchaseRequest
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import ProfileTree, ApproveUsersTree


@receiver(post_save, sender=PurchaseRequest)
def purchase_create(sender, instance, created, **kwargs):
    if created:
        author = ProfileTree.objects.get(user=instance.author)
        approvers = []

        parent = author.get_parent()
        approvers.append(parent)

        lawyer = ApproveUsersTree.get_roots()[0]
        approvers_list = [lawyer]
        approvers_list.extend(lawyer.get_descendants())
        approvers_list = [User.objects.get(username=x.user) for x in approvers_list]
        approvers_list = [ProfileTree.objects.get(user=x) for x in approvers_list]
        approvers.extend(approvers_list)

        for approver in approvers:
            PurchaseApprove.objects.create(purchase=instance, approver=approver)

    # pprint(kwargs)
    # pprint(created)

    # TODO: создать функцию ресивер для изменения закупки
