from django.urls import path
from . import views

app_name = 'purchase'
urlpatterns = [
    path('', views.PurchaseListView.as_view(), name='index'),
    # ex: /purchase/5/
    path('<int:pk>/', views.PurchaseDetailView.as_view(), name='detail'),
    path('<int:pk>/files/', views.PurchaseFiles.as_view(), name='files'),
    path('<int:pk>/update/', views.PurchaseUpdateView.as_view(), name='purchase-update'),
    path('<int:pk>/delete/', views.PurchaseDeleteView.as_view(), name='purchase-delete'),
    path('add/', views.PurchaseCreateView.as_view(), name='purchase-add'),
    path('my_purchases/', views.MyPurchasesListView.as_view(), name='my-purchases'),
    path('my_approves/', views.MyApprovesListView.as_view(), name='my-approves'),
]
