from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.edit import FormMixin
from django.contrib import messages

from users.models import ProfileTree
from .forms import ApproveForm, PurchaseCreateForm
from .models import PurchaseRequest, PurchaseApprove, PurchaseRequestFiles
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


# Create your views here.


class PurchaseListView(LoginRequiredMixin, ListView):
    model = PurchaseRequest
    template_name = 'purchase/index.html'
    context_object_name = 'purchase_list'
    ordering = ['-apply_date']

    def get_queryset(self):
        return PurchaseRequest.objects.exclude(status=PurchaseRequest.StatusChoices.CREATED).order_by('-apply_date').select_related('author')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Закупки'
        return context


class MyPurchasesListView(LoginRequiredMixin, ListView):
    model = PurchaseRequest
    template_name = 'purchase/my_purchases.html'
    context_object_name = 'purchase_list'
    ordering = ['-apply_date']

    def get_queryset(self):
        user = self.request.user  # Получаю объект текущего пользователя
        return PurchaseRequest.objects.filter(author=user).order_by('-apply_date').select_related('author')

    def get_context_data(self, **kwargs):
        context = super(MyPurchasesListView, self).get_context_data(**kwargs)
        context['title'] = 'Мои заявки на закупку'
        return context


class MyApprovesListView(LoginRequiredMixin, ListView):
    model = PurchaseRequest
    template_name = 'purchase/my_approves.html'
    context_object_name = 'purchase_list'
    ordering = ['-apply_date']

    def get_queryset(self):
        user = ProfileTree.objects.get(user=self.request.user)  # Получаю объект текущего пользователя
        if self.request.user.has_perm('purchase.can_approve'):
            req = PurchaseRequest.objects.filter(status=PurchaseRequest.StatusChoices.TO_APPROVE)
        else:
            purchase_list = PurchaseApprove.objects.select_related('purchase').filter(approver=user)
            req = PurchaseRequest.objects.filter(Q(purchaseapprove__in=purchase_list),
                                                 Q(status=PurchaseRequest.StatusChoices.TO_CONFIRM) |
                                                 Q(status=PurchaseRequest.StatusChoices.FIXED)
                                                 ).select_related('author')
        return req

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Согласование заявок'
        return context


class PurchaseDetailView(FormMixin, LoginRequiredMixin, DetailView):
    model = PurchaseRequest
    template_name = 'purchase/details.html'
    form_class = ApproveForm

    # def get_queryset(self):
    #     return PurchaseRequest.objects.filter(pk=self.kwargs['pk']).select_related('author')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        purchase = self.object
        context['title'] = purchase.title

        purchase_approves = PurchaseApprove.objects.filter(purchase=purchase).select_related('purchase', 'approver')
        context['purchase_approves'] = dict((key, key.approver.user) for key in purchase_approves)

        # Если текущий пользователь обладает разрешением на утверждение заявки
        # и заявка находится в статусе "На утверждении"
        if self.request.user.has_perm('purchase.can_approve') and purchase.status == PurchaseRequest.StatusChoices.TO_APPROVE:
            context['can_approve'] = True

        return context

    """Обработка формы в POST запросе"""

    def form_valid(self, form):
        purchase = PurchaseRequest.objects.get(pk=self.kwargs['pk'])
        if purchase.status == PurchaseRequest.StatusChoices.TO_CONFIRM or \
                purchase.status == PurchaseRequest.StatusChoices.FIXED:
            if 'approve-btn' in self.request.POST:
                user = self.request.user
                approve = PurchaseApprove.objects.get(purchase=purchase, approver__user=user)
                if approve.approver_choice == '':
                    approve.approver_choice = PurchaseApprove.ApproveChoices.values[0]
                    approve.save()
                    messages.add_message(self.request, messages.SUCCESS, f'Закупка успешно согласована')
                    purchase.is_approved()
                else:
                    messages.add_message(self.request, messages.WARNING, f'Вы уже принимали решение по данной закупке')
            elif 'reject-btn' in self.request.POST:
                user = self.request.user
                purchase = PurchaseRequest.objects.filter(pk=self.kwargs['pk']).first()
                approve_list = PurchaseApprove.objects.filter(purchase=purchase)
                approve = approve_list.get(approver__user=user)
                if approve.approver_choice == '':
                    for current in approve_list:
                        if current == approve:
                            current.approver_choice = PurchaseApprove.ApproveChoices.values[1]
                            current.save()
                            messages.add_message(self.request, messages.ERROR, f'Закупка отклонена', extra_tags='danger')
                        else:
                            current.approver_choice = ''
                            current.save()
                    purchase.status = PurchaseRequest.StatusChoices.MODIFY
                    purchase.save()
                else:
                    messages.add_message(self.request, messages.WARNING, f'Вы уже принимали решение по данной закупке')
        elif purchase.status == PurchaseRequest.StatusChoices.TO_APPROVE:
            if 'approve-btn' in self.request.POST:
                purchase.status = PurchaseRequest.StatusChoices.APPROVED
                purchase.save()
                messages.add_message(self.request, messages.SUCCESS, f'Закупка согласована')

            elif 'reject-btn' in self.request.POST:
                purchase.status = PurchaseRequest.StatusChoices.MODIFY
                purchase.save()
                messages.add_message(self.request, messages.ERROR, f'Закупка отклонена', extra_tags='danger')
        else:
            messages.add_message(self.request, messages.ERROR,
                                 f'Невозможно выполнить действие, так как закупка находится '
                                 f'в статусе {self.get_object().get_status_display()}', extra_tags='danger')

        # TODO: Реализовать сохранение комментариев к закупке
        # print(form.cleaned_data['comment'])
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        # self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse("purchase:detail", kwargs={'pk': self.kwargs['pk']})


class PurchaseCreateView(LoginRequiredMixin, CreateView):
    model = PurchaseRequest
    fields = ['title', 'goods', 'supplier', 'offer']
    form_files_class = PurchaseCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form_files = self.form_files_class()
        context['form_files'] = form_files
        return context

    def post(self, request, *args, **kwargs):
        self.object = []
        formclass = self.get_form_class()
        templatename = self.get_template_names()
        form = formclass(request.POST)
        print(request.FILES)
        form_files = self.form_files_class(request.POST, request.FILES, request=self.request)
        if form.is_valid() and form_files.is_valid():
            form.instance.author = self.request.user
            if 'confirm-btn' in self.request.POST:
                form.instance.status = PurchaseRequest.StatusChoices.TO_CONFIRM
            purchase = form.save()
            form_files.save_for(purchase)
            return HttpResponseRedirect('/')
        return render(request, templatename, {'form': form, 'form_files': form_files})

    # def form_valid(self, form):
    #     print('FORM VALID')
    #     form.instance.author = self.request.user
    #     if 'confirm-btn' in self.request.POST:
    #         form.instance.status = PurchaseRequest.StatusChoices.TO_CONFIRM
    #     return super().form_valid(form)


class PurchaseUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = PurchaseRequest
    fields = ['title', 'goods', 'supplier', 'offer']

    def form_valid(self, form):
        purchase = self.object
        if 'confirm-btn' in self.request.POST:
            if purchase.status == PurchaseRequest.StatusChoices.MODIFY:
                form.instance.status = PurchaseRequest.StatusChoices.FIXED
                approve_list = PurchaseApprove.objects.filter(purchase=purchase).select_related('approver')
                for current in approve_list:
                    current.approver_choice = ''
                    current.save()
            elif purchase.status == PurchaseRequest.StatusChoices.CREATED:
                form.instance.status = PurchaseRequest.StatusChoices.TO_CONFIRM
        return super().form_valid(form)

    def test_func(self):
        purchase = self.get_object()
        if self.request.user == purchase.author:
            return True
        return False


class PurchaseDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = PurchaseRequest
    success_url = '/purchase/'

    def test_func(self):
        purchase = self.get_object()
        if self.request.user == purchase.author:
            return True
        return False


class PurchaseFiles(LoginRequiredMixin, ListView):
    model = PurchaseRequestFiles
    template_name = 'purchase/purchase_files.html'

    def get_queryset(self):
        return PurchaseRequestFiles.objects.filter(purchase_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):

        context = super(PurchaseFiles, self).get_context_data(**kwargs)
        context['title'] = f'Файлы закупки №{self.kwargs["pk"]}'
        context['purchase'] = self.kwargs['pk']
        return context
