from django.apps import AppConfig


class PurchaseAppConfig(AppConfig):
    name = 'purchase'

    def ready(self):
        import purchase.signals